 # -*- coding: cp1251 -*-

import csv
import json

# ���������� ������ 
csv_file = 'Test.csv'
json_file = 'Test2.json'

data = [
    ["��'�", '³�', '����'],
    ['������', 19, 2],
]
# ����� ����� � csv ����
with open('Test.csv', 'w') as file:
    writer = csv.writer(file)
    writer.writerows(data)
    
with open(csv_file, 'r') as file:
    
    reader = csv.reader(file)

# ����� ����� � json ����
with open(json_file, 'w', encoding='utf-8') as json_output:
    json.dump(data, json_output, ensure_ascii=False, indent=4)

#������ ������� - ��� ���������, ���:
json_file_input = 'Test2.json'
try:
    # ���������� ����� � .json �����
    with open(json_file_input, 'r', encoding='utf-8') as json_input:
        data_from_json = json.load(json_input)

    # ��������� ����� �����
    new_data = [
        ['New', 'Data', 'Row'],
        ['123', '456', '789'],
    ]

    # �'������� ������ � ����� �����
    merged_data = data_from_json + new_data

    # ����� � .csv ����
    csv_file_output = 'Test3.csv'
    with open(csv_file_output, 'w', newline='') as csv_output:
        writer = csv.writer(csv_output)
        writer.writerows(merged_data)
    print(f'Data successfully written to {csv_file_output}!')

except Exception as e:
    print(f'Error: {e}')

#����� ������� - ��������� �����, ���:
csv_file_input = 'Test3.csv'
try:
    # ���������� ����� � CSV �����
    with open(csv_file_input, 'r') as csv_input:
        data_from_csv = csv.reader(csv_input)
        existing_data = [row for row in data_from_csv]
    # ��������� ����� �����
    new_info = [
        ['Name', 'Age', 'Kurs'],
        ['Denis', '18', '2'],
    ]
    # �'�������� ������ � ����� �����
    merged = existing_data + new_info
    # ����� ����� �� JSON �����
    json_file_path = 'Test4.json'
    with open(json_file_path, 'w') as json_file:
        json.dump(merged, json_file, indent=2)
    print(f'Data saved to {json_file_path}')
except Exception as e:
    print(f'Error: {e}')